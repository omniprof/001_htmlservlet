package com.kfwebstandard.htmlservlet.servlet;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kfwebstandard.htmlservlet.model.User;
import com.kfwebstandard.htmlservlet.persistence.UserIO;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the servlet that receives a request from the join_email_list.html
 *
 * @author Ken Fogel
 */
@WebServlet(name = "AddToEmailList", urlPatterns = {"/AddToEmailList"})
public class AddToEmailListServlet extends HttpServlet {

    // All good programmers use a Logger
    private final static Logger LOG = LoggerFactory.getLogger(AddToEmailListServlet.class);

    /**
     * Handles the HTTP <code>GET</code> method. GET is used so that you can see
     * the query string in the browser's address bar
     *
     * @param request server produced object from the user
     * @param response server produced object for replying to the user
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // get parameters from the request
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String emailAddress = request.getParameter("emailAddress");

        // store data in User object
        User user = new User(firstName, lastName, emailAddress);

        // Read the context param from web.xml
        ServletContext context = getServletContext();
        String fileName = context.getInitParameter("EmailFile2");

        // write the User object to a file
        UserIO userIO = new UserIO();
        userIO.addRecord(user, fileName);

        // send response to browser
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            displayConfirmtion(out, user);
        }
    }

    /**
     * This method handles the output back to the browser
     * 
     * @param out object that writes to the user's browser
     * @param user object with user entered data
     */
    private void displayConfirmtion(PrintWriter out, User user) {
        out.println("<!DOCTYPE html>"
                + "<html>"
                + "<head>"
                + "<title>001 Just Servlet Output</title>"
                + "<link rel=\"stylesheet\" href=\"styles/main.css\" type=\"text/css\"/>"
                + "</head>"
                + "<body>"
                + "<h1>001 - Thanks for joining our email list</h1>"
                + "<p>Here is the information that you entered:</p>"
                + "<label>Email:</label> "
                + "<span>" + user.getEmailAddress() + "</span><br>"
                + "<label>First Name:</label>"
                + "<span>" + user.getFirstName() + "</span><br>"
                + "<label>Last Name:</label>"
                + "<span>" + user.getLastName() + "</span><br>"
                + "<p>To enter another email address, click on the Return button shown below.</p>"
                + "<form action=\"join_email_list.html\" method=\"get\">"
                + "<input type=\"submit\" value=\"Return\">"
                + "</form>"
                + "<p>This email address was added to our list on " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE) + "</p>"
                + "</body>"
                + "</html>");
    }
}
